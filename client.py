#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
***
Module: scripts showing direct communication with CloudFoxy hardware via its via TCP/IP protocol.
***

 Copyright (C) Smart Arcs Ltd, registered in the United Kingdom.
 This file is owned exclusively by Smart ARcs Ltd.
 Unauthorized copying of this file, via any medium is strictly prohibited

 The code is provided under the MIT license

 Written by Smart Arcs Ltd <support@enigmabridge.com>, August 2018
"""

import socket
import time
import sys
import logging

root = logging.getLogger()
root.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

TEST_FIRST_CARD_ID = 1
TEST_NUMBER_OF_CARDS = 120
FROZEN_TIME = 5  # time is in seconds

# this is the index you want to have for the first smart card
# having different starting IDs simplifies address with more CloudFoxy boards
CARD_OFFSET = 1  # 1024

# CLOUDFOXY_IP = "localhost"
# CLOUDFOXY_IP = "192.168.42.13"
CLOUDFOXY_IP = "147.251.49.70"
UDP_PORT = 7747
TCP_PORT = 7746

USE_TCP = True


class Commands:
    # connID - 4B, card offset - 4B
    MSG_GLOBAL_CONFIG = "01002703%s004c4b401301%s0000123bF81300008131FE454A434F5076323431B7"

    MSG_GLOBAL_UNCONFIG = "01000807%s"  # strings: connID - 4B

    MSG_RESET = "01000c02%s%s"  # strings: cardID - 4B, connID - 4B

    MSG_DISABLE = "01000c08%s%s"  # strings: cardID - 4B, connID - 4B
    # MSG_SELECT = "01001e0101%s%s%s00a4040008a000000003000000"  #strings: cardID - 4B, uoID - 4B, connID - 4B
    MSG_SELECT = "01001f0101%s%s%s00a40400096D7970616330303031"  # strings: cardID - 4B, uoID - 4B, connID - 4B

    # simplest command - only returns 2 bytes - very quick
    MSG_CMD1 = "0100160101%s%s%sB069000000"  # strings: cardID - 4B, uoID - 4B, connID - 4B
    MSG_CMDM = "0100170101%s%s%s00B069000000"  # strings: cardID - 4B, uoID - 4B, connID - 4B

    # this one returns 3 bytes of a response and 2 bytes code
    MSG_CMD2 = "0100160101%s%s%sB060000000"  # strings: cardID - 4B, uoID - 4B, connID - 4B

    # a quick response - 240 bytes + 2 bytes of the status code
    MSG_CMD3 = "0100190101%s%s%sB075000003000000"  # strings: cardID - 4B, uoID - 4B, connID - 4B

    # a slow command - 2-50 seconds, it will eventually return 240 bytes + 2bytes of the status code
    MSG_CMD4 = "0100190101%s%s%sB075190003020800"  # strings: cardID - 4B, uoID - 4B, connID - 4B

    # a IO speed test command the last byte says how much data is sent back - 0xfe is max for T=1
    MSG_CMD5 = "0100160101%s%s%sB0780000fe"  # strings: cardID - 4B, uoID - 4B, connID - 4B

    # a select with as long data as possible - it should cause a 2B status code response
    MSG_CMD6 = "01010f0101%s%s%s00a40400f96D797061633030303100010203040506070809000102030405060708090" \
               "0010203040506070809000102030405060708090001020304050607080900010203040506070809000102" \
               "0304050607080900010203040506070809000102030405060708090001020304050607080900010203040" \
               "5060708090001020304050607080900010203040506070809000102030405060708090001020304050607" \
               "0809000102030405060708090001020304050607080900010203040506070809000102030405060708090" \
               "0010203040506070809000102030405060708090001020304050607080900010203040506070809000102" \
               "03040506070809"  # strings: cardID - 4B, uoID - 4B, connID - 4B

    # test for long data - it will return 2B status - 90 00
    MSG_CMD7 = "01010f0101%s%s%sb0720600f9000102030405060708090001020304050607080900010203040506070809" \
               "00010203040506070809000102030405060708090001020304050607080900010203040506070809000102" \
               "03040506070809000102030405060708090001020304050607080900010203040506070809000102030405" \
               "06070809000102030405060708090001020304050607080900010203040506070809000102030405060708" \
               "09000102030405060708090001020304050607080900010203040506070809000102030405060708090001" \
               "02030405060708090001020304050607080900010203040506070809000102030405060708090001020304" \
               "05060708"  # strings: cardID - 4B, uoID - 4B, connID - 4B

    def __init__(self):
        pass


UOID = 1200
connID = 500

print("UDP target IP: %s" % CLOUDFOXY_IP)
if USE_TCP:
    print("We talk to Foxy with TCP")
    print("TCP target port: %s" % TCP_PORT)
else:
    print("We talk to Foxy with UDP")
    print("UDP target port: %s" % UDP_PORT)

validCards = []

noResponse = True
response = None


def global_unconfig(conn_id=1234):
    """"
    This unlocks a CloudFoxy board for a new global_config command.
    conn_id - optional - an ID identifying requests and relevant response, which will copy the conn_id

    :return: "9000" if ok, other values show an error
    """
    # success = False
    attempts = 5
    no_response = True
    res_value = "0000"
    my_socket = None

    while attempts > 0 and no_response:
        attempts -= 1
        try:
            if USE_TCP:
                my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # TCP
                my_socket.connect((CLOUDFOXY_IP, TCP_PORT))
                msg_unconfig = Commands.MSG_GLOBAL_UNCONFIG % (hex(conn_id)[2:].zfill(8))
                logging.debug("Sending: %s" % msg_unconfig)
                my_socket.send(msg_unconfig)
                sock_response = my_socket.recv(1024)
                logging.debug(sock_response)
                if len(sock_response) >= 4:
                    res_value = sock_response[-4:]

                    my_socket.close()
                no_response = False
            else:
                my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
                msg_unconfig = Commands.MSG_GLOBAL_UNCONFIG % (hex(conn_id)[2:].zfill(8))
                logging.debug("Sending: %s" % msg_unconfig)

                my_socket.sendto(msg_unconfig, (CLOUDFOXY_IP, UDP_PORT))
                sock_response = my_socket.recvfrom(1024)
                logging.debug(sock_response)
                if len(sock_response) >= 4:
                    res_value = sock_response[-4:]

                no_response = False

        except socket.timeout:
            logging.debug("No answer from the server")
            # success = False
            if my_socket:
                my_socket.close()

        if no_response:
            time.sleep(2)

    if no_response:
        logging.debug("No answer from the server")
        # success = False

    return res_value  # res_value -> OK = "9000"
    # 01 0008 07 00002710
    pass


def global_config(card_offset=1, conn_id=1234):
    """"
    card_offset - 0-65535 - an offset that CloudFoxy will deduct from provided card IDs to find the correct smartcard
    conn_id - optional - an ID identifying requests and relevant response, which will copy the conn_id

    :return: "9000" if ok, other values show an error
    """
    # success = False
    attempts = 5
    no_response = True
    res_value = "0000"
    my_socket = None

    while attempts > 0 and no_response:
        attempts -= 1
        try:
            if USE_TCP:
                my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # TCP
                my_socket.connect((CLOUDFOXY_IP, TCP_PORT))
                logging.debug("Sending: %s" % (Commands.MSG_GLOBAL_CONFIG %
                                               (hex(conn_id)[2:].zfill(8), hex(card_offset)[2:].zfill(8))))
                my_socket.send(Commands.MSG_GLOBAL_CONFIG % (hex(conn_id)[2:].zfill(8), hex(card_offset)[2:].zfill(8)))
                socket_response = my_socket.recv(1024)
                logging.debug(socket_response)
                if len(socket_response) >= 4:
                    res_value = socket_response[-4:]

                    my_socket.close()
                no_response = False
            else:
                my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
                logging.debug(
                    "Sending: %s" % (Commands.MSG_GLOBAL_CONFIG %
                                     (hex(conn_id)[2:].zfill(8), hex(card_offset)[2:].zfill(8))))

                my_socket.sendto(Commands.MSG_GLOBAL_CONFIG %
                                 (hex(conn_id)[2:].zfill(8), hex(card_offset)[2:].zfill(8)),
                                 (CLOUDFOXY_IP, UDP_PORT))
                socket_response = my_socket.recvfrom(1024)
                logging.debug(socket_response)
                if len(socket_response) >= 4:
                    res_value = socket_response[-4:]

                no_response = False

        except socket.timeout:
            logging.debug("No answer from the server")
            # success = False
            if my_socket:
                my_socket.close()

        if no_response:
            time.sleep(2)

    if no_response:
        logging.debug("No answer from the server")
        # success = False

    return res_value  # res_value -> OK = "9000"


def disable(first_id, number, conn_id=1234):
    """
    This function will disable smart cards until a RESET command is sent.
    first

    :param first_id: the ID of the first smart card - it should include the offset, i.e., first card = offset + 1
    :param number: the total number smart cards to disable - starting with "first_id" card
    :param conn_id: an option conn_id to use for the first mesage - it will be incremented with each message sent
    :return:
    """

    for card_id in range(first_id, first_id + number):
        command_disable = ""
        my_socket = None
        # noinspection PyBroadException
        try:
            if USE_TCP:
                my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # TCP
                my_socket.connect((CLOUDFOXY_IP, TCP_PORT))
                command_disable = Commands.MSG_DISABLE % (hex(card_id)[2:].zfill(8), hex(conn_id)[2:].zfill(8))
                logging.debug("Sent DISABLE, card: %d, message: %s " % (card_id, command_disable))
                my_socket.send(command_disable)
                socket_response = my_socket.recv(1024)
                my_socket.close()
                if socket_response[-4:] == "9000":
                    logging.debug("DISABLE - card: %d  DISABLED OK" % card_id)
                else:
                    logging.debug("DISABLE - card: %d  ERROR %s" % (card_id, socket_response[-4:]))
            else:
                my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
                command_disable = Commands.MSG_DISABLE % (hex(card_id)[2:].zfill(8), hex(conn_id)[2:].zfill(8))
                logging.debug("Sent DISABLE, card: %d, message: %s " % (card_id, command_disable))
                my_socket.sendto(command_disable, (CLOUDFOXY_IP, UDP_PORT))
                conn_id = (conn_id + 1) % 0x10000
                socket_response = my_socket.recvfrom(1024)
                if socket_response[-4:] == "9000":
                    logging.debug("DISABLE - card: %d  DISABLED OK" % card_id)
                else:
                    logging.debug("DISABLE - card: %d  ERROR %s" % (card_id, socket_response[-4:]))
        except Exception:
            logging.debug("No answer from the server - card: %d, message %s" % (card_id, command_disable))
            # success = False
            if my_socket:
                my_socket.close()

        conn_id = (conn_id + 1) % 0x10000

    return True


def reset(first_id, number, conn_id=1234):
    """
    This function will disable smart cards until a RESET command is sent.
    first

    :param first_id: the ID of the first smart card - it should include the offset, i.e., first card = offset + 1
    :param number: the total number smart cards to disable - starting with "first_id" card
    :param conn_id: an option conn_id to use for the first mesage - it will be incremented with each message sent
    :return:
    """

    valid_cards = []

    for card_id in range(first_id, first_id + number):
        command_disable = ""
        my_socket = None
        # noinspection PyBroadException
        try:
            if USE_TCP:
                my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # TCP
                my_socket.connect((CLOUDFOXY_IP, TCP_PORT))
                command_reset = Commands.MSG_RESET % (hex(card_id)[2:].zfill(8), hex(conn_id)[2:].zfill(8))
                logging.debug("Sent RESET, card: %d, message: %s " % (card_id, command_reset))
                my_socket.send(command_reset)
                socket_response = my_socket.recv(1024)
                my_socket.close()
                if socket_response[-4:] == "9000":
                    logging.debug("RESET - card: %d  RESET OK" % card_id)
                    valid_cards.append("/%s@%d" % (CLOUDFOXY_IP, card_id))
                else:
                    logging.debug("RESET - card: %d  ERROR %s" % (card_id, socket_response[-4:]))
            else:
                my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
                command_reset = Commands.MSG_RESET % (hex(card_id)[2:].zfill(8), hex(conn_id)[2:].zfill(8))
                logging.debug("Sent RESET, card: %d, message: %s " % (card_id, command_reset))
                my_socket.sendto(command_reset, (CLOUDFOXY_IP, UDP_PORT))
                conn_id = (conn_id + 1) % 0x10000
                socket_response = my_socket.recvfrom(1024)
                if socket_response[-4:] == "9000":
                    logging.debug("RESET - card: %d  DISABLED OK" % card_id)
                    valid_cards.append("/%s@%d" % (CLOUDFOXY_IP, card_id))
                else:
                    logging.debug("RESET - card: %d  ERROR %s" % (card_id, socket_response[-4:]))
        except Exception:
            logging.debug("No answer from the server - card: %d, message %s" % (card_id, command_disable))
            # success = False
            if my_socket:
                my_socket.close()

        conn_id = (conn_id + 1) % 0x10000

    return valid_cards


def test_cmd_udp(card_list, conn_id=1234):
    if not card_list:
        logging.error("The list of smart cards is empty")
        return

    for card_reader in card_list:
        address = card_reader.split("@")
        card_id = int(address[1])
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(Commands.MSG_SELECT %
                    (hex(card_id)[2:].zfill(8), hex(UOID)[2:].zfill(8), hex(conn_id)[2:].zfill(8)),
                    (CLOUDFOXY_IP, UDP_PORT))
        conn_id = (conn_id + 1) % 0x10000
        socket_response = sock.recvfrom(1024)
        print("SELECT - card %s: %s" % (hex(card_id)[2:].zfill(3), socket_response[0]))

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(Commands.MSG_CMD4 % (hex(card_id)[2:].zfill(8), hex(UOID)[2:].zfill(8), hex(conn_id)[2:].zfill(8)),
                    (CLOUDFOXY_IP, UDP_PORT))
        conn_id = (conn_id + 1) % 0x10000
        socket_response = sock.recvfrom(1024)
        print("CMD4 - card %s: %s" % (hex(card_id)[2:].zfill(3), socket_response[0]))

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(Commands.MSG_CMD5 % (hex(card_id)[2:].zfill(8), hex(UOID)[2:].zfill(8), hex(conn_id)[2:].zfill(8)),
                    (CLOUDFOXY_IP, UDP_PORT))
        conn_id = (conn_id + 1) % 0x10000
        socket_response = sock.recvfrom(1024)
        print("CMD5 - card %s: %s" % (hex(card_id)[2:].zfill(3), socket_response[0]))

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(Commands.MSG_CMD7 % (hex(card_id)[2:].zfill(8), hex(UOID)[2:].zfill(8), hex(conn_id)[2:].zfill(8)),
                    (CLOUDFOXY_IP, UDP_PORT))
        conn_id = (conn_id + 1) % 0x10000
        socket_response = sock.recvfrom(1024)
        print("LONG CMD7 - card %s: %s" % (hex(card_id)[2:].zfill(3), socket_response[0]))


# ##############################################
# end of function definitions
# ##############################################
# ##############################################

# ###  main beginning
result = global_unconfig(conn_id=1234)
print("GLOBAL UNCONFIG - response: %s" % result)

result = global_config(card_offset=1, conn_id=1234)

print("GLOBAL CONFIG - response: %s" % result)
connID += 1

print("Trying to reset cards - from %d to %d" %
      (TEST_FIRST_CARD_ID, TEST_FIRST_CARD_ID + TEST_NUMBER_OF_CARDS - 1))

# result = disable(TEST_FIRST_CARD_ID, TEST_NUMBER_OF_CARDS)

# keep them frozen for X seconds
print("\nWaiting for %d seconds\n" % FROZEN_TIME)
remaining_sleep = FROZEN_TIME
while remaining_sleep > 0:
    sys.stdout.write(".")
    time.sleep(1)
    remaining_sleep -= 1
print("\n")

result = reset(TEST_FIRST_CARD_ID, TEST_NUMBER_OF_CARDS)

print("Valid cards")
print result

test_cmd_udp(result)

print("End")
